# BOOKSHELF APP

1. **Book Management:**
   - Sub-Feature 1: Add Book
   - Sub-Feature 2: Edit Book Information
   - Sub-Feature 3: Delete Book

2. **Book Search:**
   - Sub-Feature 1: Search by Title
   - Sub-Feature 2: Search by Author
   - Sub-Feature 3: Search by Category

3. **Book Categorization:**
   - Sub-Feature 1: Add Category
   - Sub-Feature 2: Edit Category Name
   - Sub-Feature 3: Delete Category

4. **Statistics and Reports:**
   - Sub-Feature 1: Category-wise Book Count Statistics
   - Sub-Feature 2: Borrowing and Returning History
   - Sub-Feature 3: Book Stock Reports

5. **Book Borrowing and Returning:**
   - Sub-Feature 1: Borrow a Book
   - Sub-Feature 2: Return a Book
   - Sub-Feature 3: Upcoming Return Notifications